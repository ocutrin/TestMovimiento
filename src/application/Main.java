package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
	launch(args);
    }

    static double rectWidth = 75;
    static double rectHeight = 100;
    static double dashWidth = 600;
    static double dashHeight = 400;

    @Override
    public void start(Stage stage) {

	MiRectangulo mRectangulo = new MiRectangulo();
	mRectangulo.setX((dashWidth - rectWidth) / 2);
	mRectangulo.setY((dashHeight - rectHeight) / 2);
	mRectangulo.setWidth(rectWidth);
	mRectangulo.setHeight(rectHeight);

	Pizarra pizarra = new Pizarra(mRectangulo);
	pizarra.prefWidth(dashWidth);
	pizarra.prefHeight(dashHeight);

	crearScenaYMostrar(stage, pizarra);

    }

    private static void crearScenaYMostrar(Stage stage, Pane pane) {
	stage.setScene(new Scene(pane));
	stage.setTitle("Custom Control");
	stage.setWidth(dashWidth);
	stage.setHeight(dashHeight + Pizarra.BUTTON_HEIGHT * 3);
	stage.show();
    }

}
