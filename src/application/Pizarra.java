package application;

import application.MiRectangulo.Escalar;
import application.MiRectangulo.Mover;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

public class Pizarra extends BorderPane {

    public static double BUTTON_WIDTH = 50;
    public static double BUTTON_HEIGHT = 50;
    public static double SUB_BUTTON_WIDTH = 25;
    public static double SUB_BUTTON_HEIGHT = 25;

    public Pizarra(MiRectangulo mRectangulo) {
	Group group = new Group();
	group.getChildren().addAll(mRectangulo);
	AnchorPane anchorPane = new AnchorPane();
	anchorPane.getChildren().add(group);
	setCenter(anchorPane);
	setBottom(createGridButton(mRectangulo));
    }

    private static GridPane createGridButton(MiRectangulo mRectangulo) {
	GridPane gridPane = new GridPane();
	Mover[] buttons = Mover.values();
	int cont = 0;
	for (int i = 0; i < 3; i++) {
	    for (int j = 0; j < 3; j++) {
		Region region;
		if (i != 1 || j != 1) {
		    final int ii = cont;
		    region = createButton(buttons[ii].codigo, e -> mRectangulo.mover(buttons[ii]));
		    cont++;
		} else {
		    region = createSubGridButton(mRectangulo);
		}
		GridPane.setConstraints(region, i, j);
		gridPane.getChildren().add(region);
	    }
	}
	return gridPane;
    }

    private static Button createButton(String text, EventHandler<ActionEvent> event) {
	Button button = new Button();
	button.setText(text);
	button.setOnAction(event);
	button.setPrefWidth(BUTTON_WIDTH);
	button.setPrefHeight(BUTTON_HEIGHT);
	return button;
    }

    private static GridPane createSubGridButton(MiRectangulo mRectangulo) {
	GridPane gridPane = new GridPane();
	Enum<?>[] buttons = { Mover.DERECHA, Mover.IZQUIERDA, Escalar.MAS, Escalar.MENOS };
	int cont = 0;
	for (int i = 0; i < 2; i++) {
	    for (int j = 0; j < 2; j++) {
		Button b = null;
		final int ii = cont;
		if (buttons[ii] instanceof Mover) {
		    b = createSubButton(((Mover) buttons[ii]).codigo, e -> mRectangulo.rotar(((Mover) buttons[ii])));
		}
		if (buttons[ii] instanceof Escalar) {
		    b = createSubButton(((Escalar) buttons[ii]).codigo, e -> mRectangulo.escalar(((Escalar) buttons[ii])));
		}
		cont++;
		GridPane.setConstraints(b, i, j);
		gridPane.getChildren().add(b);
	    }
	}
	return gridPane;
    }

    private static Button createSubButton(String text, EventHandler<ActionEvent> event) {
	Button button = new Button();
	button.setText(text);
	button.setOnAction(event);
	button.setPrefSize(SUB_BUTTON_WIDTH, SUB_BUTTON_HEIGHT);
	return button;
    }
}
