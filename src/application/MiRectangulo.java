package application;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class MiRectangulo extends Rectangle {

    public MiRectangulo() {
	setFill(Color.TRANSPARENT);
	setStroke(Color.RED);
    }

    public enum Mover {
	ARRIBA_IZQUIERDA("UL"), IZQUIERDA("L"), ABAJO_IZQUIERDA("DL"), ARRIBA("U"), ABAJO("D"), ARRIBA_DERECHA("UR"), DERECHA("R"), ABAJO_DERECHA("RD");
	String codigo;

	private Mover(String codigo) {
	    this.codigo = codigo;
	}
    }

    public enum Escalar {
	MAS("+"), MENOS("-");
	String codigo;

	private Escalar(String codigo) {
	    this.codigo = codigo;
	}
    }

    private double deltaAlpha = 5.0; // incremento en el angulo para un paso.
    private double deltaXY = 40.0; // incremento en x,y para un paso.
    private double anguloZero = 45; // angulo que establece el grado 0 para las traslaciones diagonales.
    private double[] escalado = { 0.25, 0.5, 1, 1.5, 2, 2.5, 3 }; // valores para el escalado.
    private int escaladoCont = 2;

    public void escalar(Escalar escTipo) {
	switch (escTipo) {
	case MAS:
	    if (escaladoCont < escalado.length - 1) {
		escaladoCont++;
		setScaleX(escalado[escaladoCont]);
		setScaleY(escalado[escaladoCont]);
	    }
	    break;
	case MENOS:
	    if (escaladoCont > 0) {
		escaladoCont--;
		setScaleX(escalado[escaladoCont]);
		setScaleY(escalado[escaladoCont]);
	    }
	    break;
	default:
	}
    }

    public void rotar(Mover direccion) {
	switch (direccion) {
	case DERECHA:
	    setRotate(getRotate() + deltaAlpha);
	    break;
	case IZQUIERDA:
	    setRotate(getRotate() - deltaAlpha);
	    break;
	default:
	}
    }

    public void mover(Mover tipo) {
	switch (tipo) {
	case ARRIBA_IZQUIERDA:
	    setX(getX() - deltaXY * cos(anguloZero + getRotate()));
	    setY(getY() - deltaXY * sin(anguloZero + getRotate()));
	    break;
	case ARRIBA_DERECHA:
	    setX(getX() + deltaXY * sin(anguloZero + getRotate()));
	    setY(getY() - deltaXY * cos(anguloZero + getRotate()));
	    break;
	case ABAJO_DERECHA:
	    setX(getX() + deltaXY * cos(anguloZero + getRotate()));
	    setY(getY() + deltaXY * sin(anguloZero + getRotate()));
	    break;
	case ABAJO_IZQUIERDA:
	    setX(getX() - deltaXY * sin(anguloZero + getRotate()));
	    setY(getY() + deltaXY * cos(anguloZero + getRotate()));
	    break;
	case IZQUIERDA:
	    setX(getX() - deltaXY);
	    break;
	case ARRIBA:
	    setY(getY() - deltaXY);
	    break;
	case DERECHA:
	    setX(getX() + deltaXY);
	    break;
	case ABAJO:
	    setY(getY() + deltaXY);
	    break;
	}
    }

    private double cos(double a) {
	return Math.cos(Math.toRadians(a)) * Math.sqrt(2);
    }

    private double sin(double a) {
	return Math.sin(Math.toRadians(a)) * Math.sqrt(2);
    }

}
